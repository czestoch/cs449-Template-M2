package recommend

import knn.{KNNBased, Neighbourhood, NormalizedDeviation, Rating}
import org.rogach.scallop._
import org.json4s.jackson.Serialization
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.log4j.Logger
import org.apache.log4j.Level
import similarity.DescriptiveStats

class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
  val data = opt[String](required = true)
  val personal = opt[String](required = true)
  val json = opt[String]()
  verify()
}

//case class Rating(user: Int, item: Int, rating: Double)

object Recommender extends App {
  // Remove these lines if encountering/debugging Spark
  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)
  val spark = SparkSession.builder()
    .master("local[1]")
    .getOrCreate()
  spark.sparkContext.setLogLevel("ERROR")

  println("")
  println("******************************************************")

  var conf = new Conf(args)
  println("Loading data from: " + conf.data())
  val dataFile = spark.sparkContext.textFile(conf.data())
  val data = dataFile.map(l => {
      val cols = l.split("\t").map(_.trim)
      Rating(cols(0).toInt, cols(1).toInt, cols(2).toDouble)
  })
  assert(data.count == 100000, "Invalid data")

  println("Loading personal data from: " + conf.personal())
  val personalFile = spark.sparkContext.textFile(conf.personal())
  // TODO: Extract ratings and movie titles
  assert(personalFile.count == 1682, "Invalid personal data")
  val personalData = personalFile.map(l => {
    val cols = l.split(",").map(_.trim)
    if (cols.length == 3) {
      (cols(0).toInt, cols(1), cols(2).toDouble)
    } else {
      (cols(0).toInt, cols(1), 0.0)
    }
  })
  val N = personalData.count().toDouble
  val myId = 944
  val personalRatings = personalData.map(x => Rating(myId, x._1, x._3))
  val potentialRecommandations = personalRatings.filter(x => x.rating == 0)
  val combinedData: RDD[Rating] = data.union(personalRatings.filter(x => x.rating != 0))
  val usersAvgRating = combinedData.groupBy(row => row.user).map(
    user => (user._1, DescriptiveStats.mean(user._2.map(row => row.rating))))
    .collectAsMap()
  val myAvgRating = usersAvgRating(myId)
  val normalizedDeviations: RDD[NormalizedDeviation] =
    combinedData.map(rating => NormalizedDeviation(rating.user,
      rating.item,
      KNNBased.getNormalizedDeviation(rating, usersAvgRating))
    )
  val cosineSimilarities: Array[(Int, Double)] = OneUserSimilarity.similarity(myId, normalizedDeviations)
  val normalizedDeviationsMap = normalizedDeviations.groupBy(row => row.item).collectAsMap()
  val Ks = Array(30, 300)
  var top5KNN: Map[Int, List[List[Any]]] = Map.empty
  for (k <- Ks){
    val kSimilarities = cosineSimilarities.sortBy(-_._2).take(k).toMap
    val top5Recommandations = potentialRecommandations.map(
      rating => (rating.item, KNNBased.predict(
        myAvgRating,
        kSimilarities,
        normalizedDeviationsMap.getOrElse(rating.item, Nil)))
    ).top(5)(Ordering[(Double, Int)].on(x => (x._2, x._1)))
    val top5 = top5Recommandations.map(x => List(x._1, personalData.filter(y => y._1 == x._1)
      .take(1)(0)._2, x._2)).toList
    top5KNN += (k -> top5)
  }


  // Save answers as JSON
  def printToFile(content: String,
                  location: String = "./answers.json") =
    Some(new java.io.PrintWriter(location)).foreach{
      f => try{
        f.write(content)
      } finally{ f.close }
  }
  conf.json.toOption match {
    case None => ;
    case Some(jsonFile) => {
      var json = "";
      {
        // Limiting the scope of implicit formats with {}
        implicit val formats = org.json4s.DefaultFormats
        val answers: Map[String, Any] = Map(

          // IMPORTANT: To break ties and ensure reproducibility of results,
          // please report the top-5 recommendations that have the smallest
          // movie identifier.

          "Q3.2.5" -> Map(
            "Top5WithK=30" -> top5KNN(30),

            "Top5WithK=300" -> top5KNN(300)

            // Discuss the differences in rating depending on value of k in the report.
          )
        )
        json = Serialization.writePretty(answers)
      }

      println(json)
      println("Saving answers in: " + jsonFile)
      printToFile(json, jsonFile)
    }
  }

  println("")
  spark.close()
}
