package similarity

import org.rogach.scallop._
import org.json4s.jackson.Serialization
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.log4j.Logger
import org.apache.log4j.Level

class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
  val train = opt[String](required = true)
  val test = opt[String](required = true)
  val json = opt[String]()
  verify()
}

case class Rating(user: Int, item: Int, rating: Double)

case class NormalizedDeviation(user: Int, item: Int, deviation: Double)

object Predictor extends App {
  // Remove these lines if encountering/debugging Spark
  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)
  val spark = SparkSession.builder()
    .master("local[1]")
    .getOrCreate()
  spark.sparkContext.setLogLevel("ERROR")

  println("")
  println("******************************************************")

  var conf = new Conf(args)
  println("Loading training data from: " + conf.train())
  val trainFile = spark.sparkContext.textFile(conf.train())
  val train = trainFile.map(l => {
    val cols = l.split("\t").map(_.trim)
    Rating(cols(0).toInt, cols(1).toInt, cols(2).toDouble)
  })
  assert(train.count == 80000, "Invalid training data")

  println("Loading test data from: " + conf.test())
  val testFile = spark.sparkContext.textFile(conf.test())
  val test = testFile.map(l => {
    val cols = l.split("\t").map(_.trim)
    Rating(cols(0).toInt, cols(1).toInt, cols(2).toDouble)
  })
  assert(test.count == 20000, "Invalid test data")

  // Cosine Similarity based predictions
  val baselineMae: Double = 0.7669
  var durationsPred: List[Long] = List()
  var durationsSimilarity: List[Long] = List()
  var predictions: Array[Double] = Array()
  var cosineSimilarities: collection.Map[Int, Map[Int, Double]] = Map.empty
  val N = test.count().toDouble
  var usersAvgRating: collection.Map[Int, Double] = Map.empty
  var normalizedDeviationsMap: collection.Map[Int, Iterable[NormalizedDeviation]] = Map.empty
  for (_ <- 1 to 5) {
    val startPred = System.currentTimeMillis()
    usersAvgRating = train.groupBy(row => row.user).map(
    user => (user._1, DescriptiveStats.mean(user._2.map(row => row.rating))))
      .collectAsMap()
    val normalizedDeviations: RDD[NormalizedDeviation] =
    train.map(rating => NormalizedDeviation(rating.user,
    rating.item,
    SimilarityBased.getNormalizedDeviation(rating, usersAvgRating))
    )
    cosineSimilarities = Cosine.similarity(test, normalizedDeviations)
    val endSimilarity = System.currentTimeMillis()
    durationsSimilarity = (endSimilarity-startPred) :: durationsSimilarity
    normalizedDeviationsMap = normalizedDeviations.groupBy(row => row.item).collectAsMap()
    predictions = test.map(rating =>
      SimilarityBased.predict(
        usersAvgRating(rating.user),
        cosineSimilarities.getOrElse(rating.user, Map.empty),
        normalizedDeviationsMap.getOrElse(rating.item, Nil)
      )).collect()
    val endPred = System.currentTimeMillis()
    durationsPred = (endPred-startPred) :: durationsPred
  }
  var i = -1
  val cosineMae = test.map(rating => {
    i+=1
    math.abs(rating.rating - predictions(i))
  }).sum / N

  val minDuration = durationsPred.min.toDouble * 1000
  val maxDuration = durationsPred.max.toDouble * 1000
  val avgDuration = DescriptiveStats.mean(durationsPred) * 1000
  val stdDuration = DescriptiveStats.stdDev(durationsPred) * 1000

  val minDurationSim = durationsSimilarity.min.toDouble * 1000
  val maxDurationSim = durationsSimilarity.max.toDouble * 1000
  val avgDurationSim = DescriptiveStats.mean(durationsSimilarity) * 1000
  val stdDurationSim = DescriptiveStats.stdDev(durationsSimilarity) * 1000

//  ===================================================================================================================

//  Jaccard based predictions
  val trainUsers = train.map(rating => rating.user).distinct().collect()
  val testUsers = test.map(row => row.user).distinct().collect()
  val usersPairs = (for (user1 <- testUsers;
                         user2 <- trainUsers)  yield (user1, user2))
  val usersItems: collection.Map[Int, Set[Int]] = train.groupBy(row => row.user)
                                                       .map(pair => (pair._1, pair._2.map(x => x.item).toSet))
                                                       .collectAsMap()
  val jaccardSimilarities = usersPairs.flatMap(pair => Jaccard.similarity(pair, usersItems))
    .groupBy(_._1).map(pair => (pair._1, pair._2.map(x => x._2).toMap))
  val jaccardMae = test.map(rating =>
    math.abs(rating.rating - SimilarityBased.predict(
      usersAvgRating(rating.user),
      jaccardSimilarities.getOrElse(rating.user, Map.empty),
      normalizedDeviationsMap.getOrElse(rating.item, Nil)
    ))
  ).sum / N

////===================================================================================================================

  val numUsers: Long = train.map(row => row.user).distinct().count()
//  2 element combinations of users as similarity is symmetric + pairs where u=v
  val numSimilarityComputations = ((numUsers * (numUsers-1)) / 2) + numUsers

////===================================================================================================================

  //  2 element combinations of users as similarity is symmetric + pairs where u=v
  val trainUsersPairs: Iterator[Array[Int]] = trainUsers.combinations(2)
  val userWithItselfPairs: Iterator[Array[Int]] = trainUsers.map(x => Array(x, x)).iterator
  val allUsersPairs: Iterator[Array[Int]] = trainUsersPairs ++ userWithItselfPairs
  val numMultiplications: Array[Int] = allUsersPairs.map(pair =>
                          {
                            val itemsU = usersItems.getOrElse(pair.head, Set[Int]())
                            val itemsV = usersItems.getOrElse(pair.last, Set[Int]())
                            itemsU.intersect(itemsV).size
                          }
                        ).toArray
  val minMultiplications = numMultiplications.min.toDouble
  val maxMultiplications = numMultiplications.max.toDouble
  val avgMultiplications = DescriptiveStats.mean(numMultiplications)
  val stdMultiplications = DescriptiveStats.stdDev(numMultiplications)

////===================================================================================================================

//  In my implementation I don't have pairs where u=v so I am adding them manually here,
//  as all of them should be 1 they all contribute to the non-zero ones
  val totalBytes = 8 * (cosineSimilarities.values.flatMap(x => x.values).count(x => x > 0) + trainUsers.length)

////===================================================================================================================

  // Save answers as JSON
  def printToFile(content: String,
                  location: String = "./answers.json") =
    Some(new java.io.PrintWriter(location)).foreach {
      f =>
        try {
          f.write(content)
        } finally {
          f.close
        }
    }

  conf.json.toOption match {
    case None => ;
    case Some(jsonFile) => {
      var json = "";
      {
        // Limiting the scope of implicit formats with {}
        implicit val formats = org.json4s.DefaultFormats
        val answers: Map[String, Any] = Map(
          "Q2.3.1" -> Map(
            "CosineBasedMae" -> cosineMae, // Datatype of answer: Double
            "CosineMinusBaselineDifference" -> (cosineMae - baselineMae) // Datatype of answer: Double
          ),

          "Q2.3.2" -> Map(
            "JaccardMae" -> jaccardMae, // Datatype of answer: Double
            "JaccardMinusCosineDifference" -> (jaccardMae - cosineMae) // Datatype of answer: Double
          ),

          "Q2.3.3" -> Map(
            // Provide the formula that computes the number of similarity computations
            // as a function of U in the report.
            "NumberOfSimilarityComputationsForU1BaseDataset" -> numSimilarityComputations // Datatype of answer: Int
          ),

          "Q2.3.4" -> Map(
            "CosineSimilarityStatistics" -> Map(
              "min" -> minMultiplications, // Datatype of answer: Double
              "max" -> maxMultiplications, // Datatype of answer: Double
              "average" -> avgMultiplications, // Datatype of answer: Double
              "stddev" -> stdMultiplications // Datatype of answer: Double
            )
          ),

          "Q2.3.5" -> Map(
            // Provide the formula that computes the amount of memory for storing all S(u,v)
            // as a function of U in the report.
            "TotalBytesToStoreNonZeroSimilarityComputationsForU1BaseDataset" -> totalBytes // Datatype of answer: Int
          ),

          "Q2.3.6" -> Map(
            "DurationInMicrosecForComputingPredictions" -> Map(
              "min" -> minDuration, // Datatype of answer: Double
              "max" -> maxDuration, // Datatype of answer: Double
              "average" -> avgDuration, // Datatype of answer: Double
              "stddev" -> stdDuration // Datatype of answer: Double
            )
            // Discuss about the time difference between the similarity method and the methods
            // from milestone 1 in the report.
          ),

          "Q2.3.7" -> Map(
            "DurationInMicrosecForComputingSimilarities" -> Map(
              "min" -> minDurationSim, // Datatype of answer: Double
              "max" -> maxDurationSim, // Datatype of answer: Double
              "average" -> avgDurationSim, // Datatype of answer: Double
              "stddev" -> stdDurationSim // Datatype of answer: Double
            ),
//            In my implementation I don't compute similarities where u=v
            "AverageTimeInMicrosecPerSuv" -> (avgDurationSim / (numSimilarityComputations-numUsers)), // Datatype of answer: Double
            "RatioBetweenTimeToComputeSimilarityOverTimeToPredict" -> (avgDurationSim / avgDuration) // Datatype of answer: Double
          )
        )
        json = Serialization.writePretty(answers)
      }

      println(json)
      println("Saving answers in: " + jsonFile)
      printToFile(json, jsonFile)
    }
  }

  println("")
  spark.close()
}
