package similarity

import org.apache.spark.rdd.RDD

object DescriptiveStats {
  val closenessThreshold = 0.5

  import Numeric.Implicits._

  def mean[T: Numeric](xs: Iterable[T]): Double = xs.sum.toDouble / xs.size

  def variance[T: Numeric](xs: Iterable[T]): Double = {
    val avg = mean(xs)
    xs.map(_.toDouble).map(a => math.pow(a - avg, 2)).sum / xs.size
  }

  def stdDev[T: Numeric](xs: Iterable[T]): Double = math.sqrt(variance(xs))

  def reportResults(results: List[Double], methodMae: Double, methodName: String): (Double, Double, Double, Double) = {
    val min = results.min
    val max = results.max
    val avg = mean(results)
    val stdd = stdDev(results)
    println(s"$methodName method MAE: $methodMae")
    println(s"$methodName  method, execution time min $min seconds")
    println(s"$methodName  method, execution time max $max seconds")
    println(s"$methodName  method, execution time avg $avg seconds")
    println(s"$methodName  method, execution time std $stdd seconds \n")
    (min, max, avg, stdd)
  }

  def reportResults(results: RDD[(Int, Double)], methodName: String): (Double, Double, Double) = {
    val min = results.map(x => x._2).min()
    val max = results.map(x => x._2).max()
    val avg = results.map(x => x._2).mean()
    println(s"$methodName  method, min $min")
    println(s"$methodName  method, max $max")
    println(s"$methodName  method, avg $avg \n")
    (min, max, avg)
  }

  def getRatioCloseToGlobalAvgRating(results: RDD[(Int, Double)], globalAverage: Double): Double = {
    results.map(x => if (math.abs(x._2 - globalAverage) < closenessThreshold) 1 else 0).sum() / results.count
  }

  def calculateGroupedMeanRating(grouped: RDD[(Int, Iterable[Rating])]): RDD[(Int, Double)] = {
    grouped.map(user => (user._1, mean(user._2.map(row => row.rating))))
  }
}
