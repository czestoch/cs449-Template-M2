package knn

import org.rogach.scallop._
import org.json4s.jackson.Serialization
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.log4j.Logger
import org.apache.log4j.Level
import similarity.DescriptiveStats

class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
  val train = opt[String](required = true)
  val test = opt[String](required = true)
  val json = opt[String]()
  verify()
}

case class Rating(user: Int, item: Int, rating: Double)

case class NormalizedDeviation(user: Int, item: Int, deviation: Double)

object Predictor extends App {
  // Remove these lines if encountering/debugging Spark
  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)
  val spark = SparkSession.builder()
    .master("local[1]")
    .getOrCreate()
  spark.sparkContext.setLogLevel("ERROR")

  println("")
  println("******************************************************")

  var conf = new Conf(args)
  println("Loading training data from: " + conf.train())
  val trainFile = spark.sparkContext.textFile(conf.train())
  val train = trainFile.map(l => {
    val cols = l.split("\t").map(_.trim)
    Rating(cols(0).toInt, cols(1).toInt, cols(2).toDouble)
  })
  assert(train.count == 80000, "Invalid training data")

  println("Loading test data from: " + conf.test())
  val testFile = spark.sparkContext.textFile(conf.test())
  val test = testFile.map(l => {
    val cols = l.split("\t").map(_.trim)
    Rating(cols(0).toInt, cols(1).toInt, cols(2).toDouble)
  })
  assert(test.count == 20000, "Invalid test data")

  // Cosine Similarity, KNN based predictions
  val baselineMae: Double = 0.7669
  val N = test.count().toDouble
  val usersAvgRating = train.groupBy(row => row.user).map(
    user => (user._1, DescriptiveStats.mean(user._2.map(row => row.rating))))
    .collectAsMap()
  val normalizedDeviations: RDD[NormalizedDeviation] =
    train.map(rating => NormalizedDeviation(rating.user,
      rating.item,
      KNNBased.getNormalizedDeviation(rating, usersAvgRating))
    )
  val cosineSimilarities: collection.Map[Int, Array[(Int, Double)]] = Neighbourhood.similarity(test, normalizedDeviations)
                                            .map(userArray => (userArray._1, userArray._2.sortBy(-_._2)))
  val normalizedDeviationsMap = normalizedDeviations.groupBy(row => row.item).collectAsMap()
  val Ks = Array(10, 30, 50, 100, 200, 300, 400, 800, 943)
  var knnMaes: List[(Int, Double, Double)] = List()
  var bytesPerK: Map[Int, Long] = Map.empty
  for (k <- Ks){
    val kSimilarities: collection.Map[Int, Map[Int, Double]] = cosineSimilarities.map(userArray =>
                                                                      (userArray._1, userArray._2.take(k).toMap))
    val bytes: Long = kSimilarities.mapValues(x => x.size).values.sum * 8
    bytesPerK += (k -> bytes)
    val knnMae = test.map(rating =>
      math.abs(rating.rating -
        KNNBased.predict(
          usersAvgRating(rating.user),
          kSimilarities.getOrElse(rating.user, Map.empty),
          normalizedDeviationsMap.getOrElse(rating.item, Nil))
     )).sum / N
    knnMaes = (k, knnMae, knnMae-baselineMae) :: knnMaes
  }
  val bestKNNMae = knnMaes.filter(x => x._3 < 0).sortBy(_._3).minBy(_._1)
  val lowestK = bestKNNMae._1
  val lowestKMae = bestKNNMae._2

//  16 GB in binary
  val ramSize: Long = 17179860388L
  val maxUsersInRam: Long = ramSize / (3*8*lowestK)


  // Save answers as JSON
  def printToFile(content: String,
                  location: String = "./answers.json") =
    Some(new java.io.PrintWriter(location)).foreach {
      f =>
        try {
          f.write(content)
        } finally {
          f.close
        }
    }

  conf.json.toOption match {
    case None => ;
    case Some(jsonFile) => {
      var json = "";
      {
        // Limiting the scope of implicit formats with {}
        implicit val formats = org.json4s.DefaultFormats
        val answers: Map[String, Any] = Map(
          "Q3.2.1" -> Map(
            // Discuss the impact of varying k on prediction accuracy on
            // the report.
            "MaeForK=10" -> knnMaes.filter(x => x._1 == 10).head._2, // Datatype of answer: Double
            "MaeForK=30" -> knnMaes.filter(x => x._1 == 30).head._2, // Datatype of answer: Double
            "MaeForK=50" -> knnMaes.filter(x => x._1 == 50).head._2, // Datatype of answer: Double
            "MaeForK=100" -> knnMaes.filter(x => x._1 == 100).head._2, // Datatype of answer: Double
            "MaeForK=200" -> knnMaes.filter(x => x._1 == 200).head._2, // Datatype of answer: Double
            "MaeForK=300" -> knnMaes.filter(x => x._1 == 300).head._2, // Datatype of answer: Double
            "MaeForK=400" -> knnMaes.filter(x => x._1 == 400).head._2, // Datatype of answer: Double
            "MaeForK=800" -> knnMaes.filter(x => x._1 == 800).head._2, // Datatype of answer: Double
            "MaeForK=943" -> knnMaes.filter(x => x._1 == 943).head._2, // Datatype of answer: Double
            "LowestKWithBetterMaeThanBaseline" -> lowestK, // Datatype of answer: Int
            "LowestKMaeMinusBaselineMae" -> (lowestKMae - baselineMae) // Datatype of answer: Double
          ),

          "Q3.2.2" -> Map(
            // Provide the formula the computes the minimum number of bytes required,
            // as a function of the size U in the report.
            // These numbers represent bytes for a by-need basis,
            // there are no similarities between train users that do not appear in the test set
            "MinNumberOfBytesForK=10" -> bytesPerK(10), // Datatype of answer: Int
            "MinNumberOfBytesForK=30" -> bytesPerK(30), // Datatype of answer: Int
            "MinNumberOfBytesForK=50" -> bytesPerK(50), // Datatype of answer: Int
            "MinNumberOfBytesForK=100" -> bytesPerK(100), // Datatype of answer: Int
            "MinNumberOfBytesForK=200" -> bytesPerK(200), // Datatype of answer: Int
            "MinNumberOfBytesForK=300" -> bytesPerK(300), // Datatype of answer: Int
            "MinNumberOfBytesForK=400" -> bytesPerK(400), // Datatype of answer: Int
            "MinNumberOfBytesForK=800" -> bytesPerK(800), // Datatype of answer: Int
            "MinNumberOfBytesForK=943" -> bytesPerK(943) // Datatype of answer: Int
          ),

          "Q3.2.3" -> Map(
            "SizeOfRamInBytes" -> ramSize, // Datatype of answer: Long
            "MaximumNumberOfUsersThatCanFitInRam" -> maxUsersInRam // Datatype of answer: Long
          )

          // Answer the Question 3.2.4 exclusively on the report.
        )
        json = Serialization.writePretty(answers)
      }

      println(json)
      println("Saving answers in: " + jsonFile)
      printToFile(json, jsonFile)
    }
  }

  println("")
  spark.close()
}
