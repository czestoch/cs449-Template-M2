package knn

import org.apache.spark.rdd.RDD

case class Similarity(user_u: Int, user_v: Int, similarity: Double)

case class PartialSimilarity(user: Int, item: Int, similarity: Double)

object Neighbourhood {

  def similarity(test: RDD[Rating],
                 normalizedDeviations: RDD[NormalizedDeviation]): collection.Map[Int, Array[(Int, Double)]] = {
    val testUsers = test.map(row => row.user).distinct().collect().toSet
    //  Calculate r_horn for all u,i pairs
    val partialSimilarities: Array[PartialSimilarity] = normalizedDeviations.groupBy(row => row.user)
      .flatMap(userItems => {
        val denominator = math.sqrt(userItems._2.map(deviation => math.pow(deviation.deviation, 2)).sum)
        userItems._2.map(deviation => PartialSimilarity(userItems._1, deviation.item, deviation.deviation / denominator))
      }).collect()
    //  Multiple r_horn u i with r_horn v i
    val multiplications: Iterable[((Int, Int), Double)] = partialSimilarities
      .groupBy(row => row.item)
      .flatMap(itemSimilarities => {
        val block = itemSimilarities._2.zipWithIndex
        for ((partialSim1, outerIndex) <- block;
             (partialSim2, innerIndex) <- block if innerIndex > outerIndex
          && (testUsers.contains(partialSim1.user) || testUsers.contains(partialSim2.user)))
          yield
            Array(
              ((partialSim1.user, partialSim2.user),
                partialSim1.similarity * partialSim2.similarity),
              ((partialSim2.user, partialSim1.user),
                partialSim1.similarity * partialSim2.similarity)
            )
      }).flatten
    val cosineSimilarities: collection.Map[Int, Array[(Int, Double)]] = multiplications.groupBy(_._1)
      .map(x => {
        (x._1, x._2.map(y => y._2).sum)
      }).groupBy(_._1._1)
      .map(x => (
        x._1,
        x._2.map(y => (y._1._2, y._2)).toArray
      ))
    cosineSimilarities
  }
}
