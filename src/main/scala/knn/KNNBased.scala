package knn

object KNNBased {
  def predict(userAvgRating: Double,
              similarities: Map[Int, Double],
              itemNormalizedDeviations: Iterable[NormalizedDeviation]
             ): Double = {

    if (itemNormalizedDeviations.isEmpty){
      userAvgRating
    }
    else {
      val usersThatRatedItem = itemNormalizedDeviations.map(row => row.user)
      val userWeightedDeviation: Double = getUserWeightedDeviation(usersThatRatedItem,
        similarities, itemNormalizedDeviations)
      userAvgRating + userWeightedDeviation * scale(userAvgRating + userWeightedDeviation, userAvgRating)
    }
  }

  def getUserWeightedDeviation(usersThatRatedItem: Iterable[Int],
                               similarities: Map[Int, Double],
                               itemNormalizedDeviations: Iterable[NormalizedDeviation]): Double = {
    var denominator = 0.0
    val numerator = usersThatRatedItem.map(user_v => {
      val similarity = similarities.getOrElse(user_v, 0.0)
      if (similarity == 0.0) {
        0.0
      }
      else {
        val deviation = itemNormalizedDeviations.filter(row => row.user == user_v).head.deviation
        denominator += math.abs(similarity)
        similarity * deviation
      }
    }).sum
    if (denominator > 0.0) {
      numerator / denominator
    }
    else {
      0.0
    }
  }

  def getNormalizedDeviation(rating:Rating, usersAvgRating: collection.Map[Int, Double]): Double = {
    val userAvgRating: Double = usersAvgRating(rating.user)
    (rating.rating - userAvgRating) / scale(rating.rating, userAvgRating)
  }

  def scale(x: Double, userAvgRating: Double): Double = {
    if (x > userAvgRating)
      5 - userAvgRating
    else if (x < userAvgRating)
      userAvgRating - 1
    else
      1
  }
}